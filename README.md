# UWS Learning Guide in LaTeX

This directory contains the markup for the UWS learning guide standard format.

To build the PDF of the learning guide, run make:
    make

