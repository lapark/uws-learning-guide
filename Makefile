TEX := xelatex
PROJ := uws_leaning_guide.pdf

proj:	${PROJ}

%.pdf:  %.tex
	${TEX} $*
